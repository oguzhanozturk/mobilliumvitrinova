package com.mobillium.vitrinova.di.module

import dagger.Module

@Module(
    includes = [
        ViewModelModule::class
    ]
)
class AppModule {


}