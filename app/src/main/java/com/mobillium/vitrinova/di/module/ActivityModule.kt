package com.mobillium.vitrinova.di.module

import com.mobillium.vitrinova.ui.collection.CollectionActivity
import com.mobillium.vitrinova.ui.home.HomeActivity
import com.mobillium.vitrinova.ui.product.ProductActivity
import com.mobillium.vitrinova.ui.shop.ShopActivity
import com.mobillium.vitrinova.ui.splash.SplashActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {

    @ContributesAndroidInjector
    abstract fun bindSplashActivity(): SplashActivity

    @ContributesAndroidInjector
    abstract fun bindHomeActivity(): HomeActivity

    @ContributesAndroidInjector
    abstract fun bindProductsActivity(): ProductActivity

    @ContributesAndroidInjector
    abstract fun bindCollectionDetailActivity(): CollectionActivity

    @ContributesAndroidInjector
    abstract fun bindShopDetailActivity(): ShopActivity

}