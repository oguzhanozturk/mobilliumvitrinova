package com.mobillium.vitrinova.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.mobillium.vitrinova.di.ViewModelFactory
import com.mobillium.vitrinova.di.key.ViewModelKey
import com.mobillium.vitrinova.ui.collection.CollectionViewModel
import com.mobillium.vitrinova.ui.home.HomeViewModel
import com.mobillium.vitrinova.ui.product.ProductViewModel
import com.mobillium.vitrinova.ui.shop.ShopViewModel
import com.mobillium.vitrinova.ui.splash.SplashViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(SplashViewModel::class)
    abstract fun bindsSplashViewModel(viewModel: SplashViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    abstract fun bindsHomeViewModel(viewModel: HomeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ProductViewModel::class)
    abstract fun bindsProductsViewModel(viewModel: ProductViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CollectionViewModel::class)
    abstract fun bindsCollectionDetailViewModel(viewModel: CollectionViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ShopViewModel::class)
    abstract fun bindsShopDetailViewModel(viewModel: ShopViewModel): ViewModel
}