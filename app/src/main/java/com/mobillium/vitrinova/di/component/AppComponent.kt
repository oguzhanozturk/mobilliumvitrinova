package com.mobillium.vitrinova.di.component

import android.app.Application
import com.mobillium.vitrinova.MobiliumApp
import com.mobillium.vitrinova.di.module.ActivityModule
import com.mobillium.vitrinova.di.module.AppModule
import com.mobillium.vitrinova.di.module.NetworkModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton


@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        NetworkModule::class,
        ActivityModule::class,
        AppModule::class
    ]
)
interface AppComponent : AndroidInjector<MobiliumApp> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(app: Application)
}