package com.mobillium.vitrinova

import com.mobillium.vitrinova.di.component.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

class MobiliumApp : DaggerApplication() {

    override fun onCreate() {
        super.onCreate()
    }

    override fun applicationInjector(): AndroidInjector<out MobiliumApp> {
        return DaggerAppComponent.builder().application(this).build()
    }

}