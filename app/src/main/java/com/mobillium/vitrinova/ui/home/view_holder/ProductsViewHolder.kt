package com.mobillium.vitrinova.ui.home.view_holder

import androidx.lifecycle.MutableLiveData
import com.mobillium.vitrinova.ui.base.BaseViewHolder
import com.mobillium.vitrinova.databinding.CellHorizantalViewBinding
import com.mobillium.vitrinova.network.model.MainResponse
import com.mobillium.vitrinova.ui.home.adapter.HomeAdapter
import com.mobillium.vitrinova.ui.home.adapter.ProductAdapter

class ProductsViewHolder(
    val binding: CellHorizantalViewBinding,
    val liveData: MutableLiveData<HomeAdapter.State>
) : BaseViewHolder<ArrayList<MainResponse>>(binding.root) {
    override fun bind(data: ArrayList<MainResponse>) {
        binding.cellHorizantalViewRecyclerView.adapter = ProductAdapter(data[1].products!!)
        binding.cellHorizantalViewTitleTextView.text = data[1].title

        binding.cellHorizantalViewAllTextView.setOnClickListener {
            liveData.value = HomeAdapter.State.OnAllClick(data, data[1].type)
        }
    }

}