package com.mobillium.vitrinova.ui.home.adapter

import com.mobillium.vitrinova.R
import com.mobillium.vitrinova.databinding.CellEditorShopProductItemBinding
import com.mobillium.vitrinova.ui.base.BaseAdapter
import com.mobillium.vitrinova.network.model.PopularProduct

class ShopProductAdapter(
    popularProduct: ArrayList<PopularProduct>
) : BaseAdapter<CellEditorShopProductItemBinding, PopularProduct>(R.layout.cell_editor_shop_product_item) {

    init {
        items = popularProduct
    }

    override fun bindView(
        binding: CellEditorShopProductItemBinding,
        item: PopularProduct?,
        adapterPosition: Int
    ) {
        binding.image = item?.images!![0]

    }

}