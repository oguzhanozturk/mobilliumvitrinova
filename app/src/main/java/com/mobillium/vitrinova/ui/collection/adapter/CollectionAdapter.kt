package com.mobillium.vitrinova.ui.collection.adapter

import com.mobillium.vitrinova.R
import com.mobillium.vitrinova.ui.base.BaseAdapter
import com.mobillium.vitrinova.databinding.CellCollectionItemBinding
import com.mobillium.vitrinova.network.model.Collection

class CollectionAdapter (
    collections: ArrayList<Collection>
): BaseAdapter<CellCollectionItemBinding, Collection>(R.layout.cell_collection_item) {

    init {
        items = collections
    }

    override fun bindView(binding: CellCollectionItemBinding, item: Collection?, adapterPosition: Int) {
        binding.collections = item
        binding.image = item!!.cover
    }

}