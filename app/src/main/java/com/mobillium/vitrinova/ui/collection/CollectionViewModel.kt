package com.mobillium.vitrinova.ui.collection

import android.os.Bundle
import com.mobillium.vitrinova.ui.base.BaseViewModel
import com.mobillium.vitrinova.network.model.MainResponse
import javax.inject.Inject

class CollectionViewModel @Inject constructor() : BaseViewModel(){

    var mainResponse : ArrayList<MainResponse>? = null
    override fun handleIntent(extras: Bundle) {
        mainResponse = extras.getParcelableArrayList(CollectionActivity.MAIN_RESPONSE) ?: arrayListOf()
    }
}