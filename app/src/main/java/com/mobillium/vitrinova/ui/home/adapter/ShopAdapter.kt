package com.mobillium.vitrinova.ui.home.adapter

import android.view.View
import com.bumptech.glide.Glide
import com.mobillium.vitrinova.R
import com.mobillium.vitrinova.databinding.CellNewShopItemBinding
import com.mobillium.vitrinova.ui.base.BaseAdapter
import com.mobillium.vitrinova.network.model.Shop

class ShopAdapter (
    shops: ArrayList<Shop>
): BaseAdapter<CellNewShopItemBinding, Shop>(R.layout.cell_new_shop_item) {

    init {
        items = shops
    }
    override fun bindView(binding: CellNewShopItemBinding, item: Shop?, adapterPosition: Int) {
        binding.shop = item

        if (item?.cover != null) {
            Glide.with(binding.root.context).load(item.cover?.url).into(binding.cellNewShopsItemImageView)
        }else {
            Glide.with(binding.root.context).load(R.mipmap.ic_launcher_round).into(binding.cellNewShopsItemImageView)
        }

        if (item?.logo != null){
            binding.cellNewShopsItemNullLogoCardView.visibility = View.GONE
            Glide.with(binding.root.context).load(item.logo?.url).into(binding.cellNewShopsItemUserImageView)
        }else {
            binding.cellNewShopsItemNullLogoCardView.visibility = View.VISIBLE
            binding.cellNewShopsNullLogoTextView.text = item?.name?.substring(0,1)
        }
    }

}