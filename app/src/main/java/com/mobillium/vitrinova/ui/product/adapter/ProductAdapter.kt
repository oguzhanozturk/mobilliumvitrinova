package com.mobillium.vitrinova.ui.product.adapter

import com.mobillium.vitrinova.R
import com.mobillium.vitrinova.ui.base.BaseAdapter
import com.mobillium.vitrinova.databinding.CellProductDetailItemBinding
import com.mobillium.vitrinova.network.model.Product

class ProductAdapter(
    products: ArrayList<Product>
) : BaseAdapter<CellProductDetailItemBinding, Product>(R.layout.cell_product_detail_item) {

    init {
        items = products
    }

    override fun bindView(
        binding: CellProductDetailItemBinding,
        item: Product?,
        adapterPosition: Int
    ) {
        binding.product = item
        binding.image = item!!.images!![0]
    }
}
