package com.mobillium.vitrinova.ui.home

import android.os.Bundle
import androidx.lifecycle.MutableLiveData
import com.mobillium.vitrinova.ui.base.BaseViewModel
import com.mobillium.vitrinova.network.ApiDataManager
import com.mobillium.vitrinova.network.model.MainResponse
import javax.inject.Inject

class HomeViewModel @Inject constructor(private val apiDataManager: ApiDataManager) : BaseViewModel() {

    var liveData = MutableLiveData<State>()
    var mainResponse : ArrayList<MainResponse>? = null
    override fun handleIntent(extras: Bundle) {
        mainResponse = extras.getParcelableArrayList(HomeActivity.MAIN_RESPONSE) ?: arrayListOf()
    }

    fun discover() {
        apiDataManager.discover().sendRequest {
            liveData.postValue(State.onLoadingState(it))
        }
    }

    sealed class State{
        data class onLoadingState(val mainResponse: ArrayList<MainResponse>): State()
    }
}