package com.mobillium.vitrinova.ui.product

import android.app.Activity
import android.content.Intent
import androidx.recyclerview.widget.GridLayoutManager
import com.mobillium.vitrinova.R
import com.mobillium.vitrinova.databinding.ActivityProductBinding
import com.mobillium.vitrinova.ui.base.BaseActivity
import com.mobillium.vitrinova.network.model.MainResponse
import com.mobillium.vitrinova.ui.product.adapter.ProductAdapter

class ProductActivity(
    override val layoutResourceId: Int = R.layout.activity_product,
    override val classTypeOfViewModel: Class<ProductViewModel> = ProductViewModel::class.java
) : BaseActivity<ActivityProductBinding, ProductViewModel>() {

    override fun initialize() {

        binding.title = viewModel.mainResponse!![1].title

        val adapter = ProductAdapter(
            viewModel.mainResponse!![1].products!!
        )

        binding.activityProductsRecyclerView.layoutManager = GridLayoutManager(this, 2)
        binding.activityProductsRecyclerView.adapter = adapter

        binding.productDetailToolbarBackButton.setOnClickListener {
            onBackPressed()
        }
    }

    companion object {
        const val EXTRA_SERVICE_ITEM = "com.mobillium.interview.ui.products.extra_service_item"
        fun start(activity: Activity, mainResponse: ArrayList<MainResponse>) {
            activity.startActivity(Intent(activity, ProductActivity::class.java).apply {
                putParcelableArrayListExtra(EXTRA_SERVICE_ITEM, mainResponse)
            })
        }
    }
}
