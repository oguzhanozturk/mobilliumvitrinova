package com.mobillium.vitrinova.ui.collection

import android.app.Activity
import android.content.Intent
import com.mobillium.vitrinova.R
import com.mobillium.vitrinova.databinding.ActivityCollectionBinding
import com.mobillium.vitrinova.ui.base.BaseActivity
import com.mobillium.vitrinova.network.model.MainResponse
import com.mobillium.vitrinova.ui.collection.adapter.CollectionAdapter

class CollectionActivity (
    override val layoutResourceId: Int = R.layout.activity_collection,
    override val classTypeOfViewModel: Class<CollectionViewModel> = CollectionViewModel::class.java
) : BaseActivity<ActivityCollectionBinding, CollectionViewModel>()  {

    override fun initialize() {
        binding.title = viewModel.mainResponse!![3].title

        val adapter = CollectionAdapter(
            viewModel.mainResponse!![3].collections!!
        )
        binding.activityCollectionRecyclerView.adapter = adapter

        binding.collectionDetailToolbarBackButton.setOnClickListener {
            onBackPressed()
        }
    }

    companion object {
        const val MAIN_RESPONSE = "MAIN_RESPONSE"
        fun start(activity: Activity, mainResponse: ArrayList<MainResponse>) {
            activity.startActivity(Intent(activity, CollectionActivity::class.java).apply {
                putParcelableArrayListExtra(MAIN_RESPONSE, mainResponse)
            })
        }
    }

}
