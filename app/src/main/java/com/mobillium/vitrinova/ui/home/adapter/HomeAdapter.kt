package com.mobillium.vitrinova.ui.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.mobillium.vitrinova.R
import com.mobillium.vitrinova.databinding.CellEditorShopViewPagerItemBinding
import com.mobillium.vitrinova.ui.base.BaseViewHolder
import com.mobillium.vitrinova.databinding.CellFeaturedItemBinding
import com.mobillium.vitrinova.databinding.CellHorizantalViewBinding
import com.mobillium.vitrinova.network.model.MainResponse
import com.mobillium.vitrinova.ui.home.view_holder.*

class HomeAdapter constructor(
    private val mainResponse: ArrayList<MainResponse>
    /*private val featuredList: ArrayList<Featured>,
    private val productsList: ArrayList<Product>,
    private val categoriesList: ArrayList<Category>,
    private val collectionsList: ArrayList<Collection>,
    private val shopsList: ArrayList<Shop>*/
) : RecyclerView.Adapter<BaseViewHolder<*>>() {

    val liveData = MutableLiveData<State>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
        val layoutInflater = LayoutInflater.from(parent.context)
        return HomeListType.values()[viewType].onCreateViewHolder(
            parent,
            layoutInflater,
            liveData
        )

    }

    override fun getItemViewType(position: Int): Int {
        return when (position) {
            0 -> HomeListType.FEATURED.viewType()
            1 -> HomeListType.PRODUCT.viewType()
            2 -> HomeListType.CATEGORIES.viewType()
            3 -> HomeListType.COLLECTIONS.viewType()
            4 -> HomeListType.EDITOR_SHOPS.viewType()
            else -> HomeListType.NEW_SHOPS.viewType()
        }
    }

    override fun getItemCount(): Int {
        return mainResponse.size
    }

    override fun onBindViewHolder(holder: BaseViewHolder<*>, position: Int) {
        when (holder) {
            is FeaturedViewHolder -> holder.bind(mainResponse)
            is ProductsViewHolder -> holder.bind(mainResponse)
            is CategoriesViewHolder -> holder.bind(mainResponse)
            is CollectionsViewHolder -> holder.bind(mainResponse)
            is EditorShopsViewHolder -> holder.bind(mainResponse)
            is NewShopsViewHolder -> holder.bind(mainResponse)
        }
    }

    enum class HomeListType {
        FEATURED {
            override fun onCreateViewHolder(
                parent: ViewGroup,
                layoutInflater: LayoutInflater,
                liveData: MutableLiveData<State>
            ): BaseViewHolder<*> {
                val binding = DataBindingUtil.inflate<CellFeaturedItemBinding>(
                    layoutInflater,
                    R.layout.cell_featured_item,
                    parent,
                    false
                )
                return FeaturedViewHolder(binding)
            }
        },
        PRODUCT {
            override fun onCreateViewHolder(
                parent: ViewGroup,
                layoutInflater: LayoutInflater,
                liveData: MutableLiveData<State>
            ): BaseViewHolder<*> {
                val binding = DataBindingUtil.inflate<CellHorizantalViewBinding>(
                    layoutInflater,
                    R.layout.cell_horizantal_view,
                    parent,
                    false
                )
                return ProductsViewHolder(binding, liveData)
            }
        },
        CATEGORIES {
            override fun onCreateViewHolder(
                parent: ViewGroup,
                layoutInflater: LayoutInflater,
                liveData: MutableLiveData<State>
            ): BaseViewHolder<*> {
                val binding = DataBindingUtil.inflate<CellHorizantalViewBinding>(
                    layoutInflater,
                    R.layout.cell_horizantal_view,
                    parent,
                    false
                )
                return CategoriesViewHolder(binding)
            }
        },
        COLLECTIONS {
            override fun onCreateViewHolder(
                parent: ViewGroup,
                layoutInflater: LayoutInflater,
                liveData: MutableLiveData<State>
            ): BaseViewHolder<*> {
                val binding = DataBindingUtil.inflate<CellHorizantalViewBinding>(
                    layoutInflater,
                    R.layout.cell_horizantal_view,
                    parent,
                    false
                )
                return CollectionsViewHolder(binding, liveData)
            }
        },
        EDITOR_SHOPS {
            override fun onCreateViewHolder(
                parent: ViewGroup,
                layoutInflater: LayoutInflater,
                liveData: MutableLiveData<State>
            ): BaseViewHolder<*> {
                val binding = DataBindingUtil.inflate<CellEditorShopViewPagerItemBinding>(
                    layoutInflater,
                    R.layout.cell_editor_shop_view_pager_item,
                    parent,
                    false
                )
                return EditorShopsViewHolder(binding, liveData)
            }
        },
        NEW_SHOPS {
            override fun onCreateViewHolder(
                parent: ViewGroup,
                layoutInflater: LayoutInflater,
                liveData: MutableLiveData<State>
            ): BaseViewHolder<*> {
                val binding = DataBindingUtil.inflate<CellHorizantalViewBinding>(
                    layoutInflater,
                    R.layout.cell_horizantal_view,
                    parent,
                    false
                )
                return NewShopsViewHolder(binding,liveData)
            }
        };

        abstract fun onCreateViewHolder(
            parent: ViewGroup,
            layoutInflater: LayoutInflater,
            liveData: MutableLiveData<State>
        ): BaseViewHolder<*>

        fun viewType(): Int = ordinal
    }

    sealed class State {
        data class OnAllClick(val mainResponse: ArrayList<MainResponse>, val type: String? = "") : State()
    }

}