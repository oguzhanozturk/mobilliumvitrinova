package com.mobillium.vitrinova.ui.home

import android.app.Activity
import android.content.Intent
import android.speech.RecognizerIntent
import com.mobillium.vitrinova.R
import com.mobillium.vitrinova.databinding.ActivityHomeBinding
import com.mobillium.vitrinova.ui.base.BaseActivity
import com.mobillium.vitrinova.network.model.MainResponse
import com.mobillium.vitrinova.ui.collection.CollectionActivity
import com.mobillium.vitrinova.ui.home.adapter.HomeAdapter
import com.mobillium.vitrinova.ui.product.ProductActivity
import com.mobillium.vitrinova.ui.shop.ShopActivity
import com.mobillium.vitrinova.util.ListType
import com.mobillium.vitrinova.util.extension.observe

class HomeActivity(
    override val layoutResourceId: Int = R.layout.activity_home,
    override val classTypeOfViewModel: Class<HomeViewModel> = HomeViewModel::class.java
) : BaseActivity<ActivityHomeBinding, HomeViewModel>() {

    private val VOICE_SEARCH_CODE = 12110
    lateinit var adapter: HomeAdapter

    override fun initialize() {
        adapter = HomeAdapter(viewModel.mainResponse!!)
        binding.homeRecyclerView.adapter = adapter

        binding.activityHomeSwipeRefreshLayout.setOnRefreshListener {
            binding.activityHomeSwipeRefreshLayout.isRefreshing=false
            viewModel.discover()
        }

        binding.mic.setOnClickListener {
            openMic()
        }

        observe(viewModel.liveData, ::onStateChanged)
        observe(adapter.liveData, ::onAdapterStateChange)
    }

    private fun openMic(){
        try {
            val intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM)
            startActivityForResult(intent, VOICE_SEARCH_CODE)
        } catch (_: Exception) {

        }
    }

    private fun onAdapterStateChange(state: HomeAdapter.State) {
        when (state) {
            is HomeAdapter.State.OnAllClick -> goToAllProducts(
                state.mainResponse,
                state.type
            )
        }
    }

    private fun onStateChanged(state: HomeViewModel.State) {
        when (state) {
            is HomeViewModel.State.onLoadingState -> {
                adapter = HomeAdapter(state.mainResponse)
                binding.homeRecyclerView.adapter = adapter
                binding.homeRecyclerView.adapter!!.notifyDataSetChanged()
                observe(adapter.liveData, ::onAdapterStateChange)
            }
        }
    }

    private fun goToAllProducts(mainResponse: ArrayList<MainResponse>, type: String?) {
        when (type) {
            ListType.COLLECTIONS.type -> CollectionActivity.start(
                this,
                mainResponse
            )
            ListType.NEW_SHOPS.type -> ShopActivity.start(this, mainResponse, type)
            ListType.EDITOR_SHOPS.type -> ShopActivity.start(
                this,
                mainResponse,
                type
            )
            ListType.NEW_PRODUCTS.type -> ProductActivity.start(this, mainResponse)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == VOICE_SEARCH_CODE && resultCode == Activity.RESULT_OK) {
            val matches = data?.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS) ?: ArrayList()
            if (matches.size != 0) {
                binding.search.setText(matches[0])
                binding.search.setSelection(binding.search.text.length)
            }
        }
    }

    companion object {
        const val MAIN_RESPONSE = "MAIN_RESPONSE"
        fun start(activity: Activity, mainResponse: ArrayList<MainResponse>) {
            activity.startActivity(Intent(activity, HomeActivity::class.java).apply {
                putParcelableArrayListExtra(MAIN_RESPONSE, mainResponse)
            })
        }
    }
}
