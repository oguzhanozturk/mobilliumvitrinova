package com.mobillium.vitrinova.ui.home.adapter

import com.mobillium.vitrinova.R
import com.mobillium.vitrinova.databinding.CellEditorShopItemBinding
import com.mobillium.vitrinova.ui.base.BaseAdapter
import com.mobillium.vitrinova.network.model.Shop


class EditorShopAdapter(
    shops: ArrayList<Shop>
) : BaseAdapter<CellEditorShopItemBinding, Shop>(R.layout.cell_editor_shop_item) {

    init {
        items = shops
    }

    override fun bindView(binding: CellEditorShopItemBinding, item: Shop?, adapterPosition: Int) {
        binding.isLastPosition = adapterPosition == (items!!.size - 1)
        binding.editorShops = item
        binding.cover = item?.cover
        binding.logo = item?.logo

        binding.cellEditorShopsPopularProductRecyclerView.adapter =
            ShopProductAdapter(items!![adapterPosition].popular_products!!)
    }

}