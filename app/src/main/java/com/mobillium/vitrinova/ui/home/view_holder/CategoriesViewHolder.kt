package com.mobillium.vitrinova.ui.home.view_holder

import android.view.View
import com.mobillium.vitrinova.ui.base.BaseViewHolder
import com.mobillium.vitrinova.databinding.CellHorizantalViewBinding
import com.mobillium.vitrinova.network.model.MainResponse
import com.mobillium.vitrinova.ui.home.adapter.CategoryAdapter

class CategoriesViewHolder(
    val binding: CellHorizantalViewBinding
) : BaseViewHolder<ArrayList<MainResponse>>(binding.root) {

    override fun bind(data: ArrayList<MainResponse>) {
        binding.cellHorizantalViewRecyclerView.adapter = CategoryAdapter(data[2].categories!!)
        binding.cellHorizantalViewTitleTextView.text = data[2].title
        binding.cellHorizantalViewAllTextView.visibility = View.GONE
    }

}