package com.mobillium.vitrinova.ui.shop.adapter

import android.view.View
import com.bumptech.glide.Glide
import com.mobillium.vitrinova.R
import com.mobillium.vitrinova.databinding.CellShopItemBinding
import com.mobillium.vitrinova.ui.base.BaseAdapter
import com.mobillium.vitrinova.network.model.Shop

class ShopAdapter (
    shops: ArrayList<Shop>
): BaseAdapter<CellShopItemBinding, Shop>(R.layout.cell_shop_item) {

    init {
        items = shops
    }
    override fun bindView(binding: CellShopItemBinding, item: Shop?, adapterPosition: Int) {
        binding.shop = item

        if (item?.cover != null) {
            Glide.with(binding.root.context).load(item.cover?.url).into(binding.activityShopsItemImageView)
        }else {
            Glide.with(binding.root.context).load(R.mipmap.ic_launcher_round).into(binding.activityShopsItemImageView)
        }

        if (item?.logo != null){
            binding.activityShopsItemNullLogoCardView.visibility = View.GONE
            Glide.with(binding.root.context).load(item.logo?.url).into(binding.activityShopsItemUserImageView)
        }else {
            binding.activityShopsItemNullLogoCardView.visibility = View.VISIBLE
            binding.activityShopsNullLogoTextView.text = item?.name?.substring(0,1)
        }
    }

}