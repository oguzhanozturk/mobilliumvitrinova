package com.mobillium.vitrinova.ui.base

import android.app.Activity
import android.os.AsyncTask
import android.os.Environment
import android.util.Log
import com.google.gson.Gson
import com.mobillium.vitrinova.network.service.ApiServices
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.Retrofit
import java.io.*
import javax.inject.Inject


open class BaseServicesImp @Inject constructor(
    private val retrofit: Retrofit,
    private val gson: Gson
) : BaseServices {

    fun <T> getRequest(
        callback: ServiceCallback<T>,
        observable: () -> Observable<T>
    ): Disposable = observable()
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(onNext(callback)) {
            callback.onError(NetworkError(it).errorCode, NetworkError(
                    it
                ).appErrorMessage)
        }

    fun <T> onNext(callback: ServiceCallback<T>): (T) -> Unit = {
        callback.onSuccess(it)
    }

    fun downloadRequest(
        activity: Activity,
        callback: ServiceCallback<String>,
        observable: () -> Observable<Response<ResponseBody>>
    ): Disposable = observable().subscribeOn(Schedulers.io()).subscribe {
        object : AsyncTask<Void?, Void?, Void?>() {
            override fun doInBackground(vararg params: Void?): Void? {


                callback.onSuccess(it.body()?.let { it1 -> writeResponseBodyToDisk(activity, it1) })
                return null
            }


        }.execute()
    }

    open fun writeResponseBodyToDisk(activity: Activity, body: ResponseBody): String {
        return try {
            // todo change the file location/name according to your needs
            var filename = "file"
            var file = File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
                    .absolutePath + File.separator + "temmuz"
            )
            if (!file.exists()) {
                file.mkdir()
            }
            file = File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
                    .absolutePath + File.separator + "temmuz" + File.separator + filename + ".2dmap"
            )
            var inputStream: InputStream? = null
            var outputStream: OutputStream? = null
            try {
                val fileReader = ByteArray(4096)
                val fileSize = body.contentLength()
                var fileSizeDownloaded: Long = 0
                inputStream = body.byteStream()
                outputStream = FileOutputStream(file)
                while (true) {
                    val read = inputStream.read(fileReader)
                    if (read == -1) {
                        break
                    }
                    outputStream.write(fileReader, 0, read)
                    fileSizeDownloaded += read.toLong()
                }
                outputStream.flush()
                return file.absoluteFile.absolutePath
            } catch (e: IOException) {
                Log.e("Hata", e.localizedMessage)
                "false"
            } finally {
                inputStream?.close()
                outputStream?.close()
            }
        } catch (e: IOException) {
            Log.e("Hata", e.localizedMessage)
            "false"
        }
    }

    override fun getApiServices(): ApiServices {
        return retrofit.create(ApiServices::class.java)
    }

}