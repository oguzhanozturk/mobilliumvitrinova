package com.mobillium.vitrinova.ui.home.view_holder

import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.mobillium.vitrinova.databinding.CellEditorShopViewPagerItemBinding
import com.mobillium.vitrinova.ui.base.BaseViewHolder
import com.mobillium.vitrinova.network.model.MainResponse
import com.mobillium.vitrinova.ui.home.adapter.EditorShopAdapter
import com.mobillium.vitrinova.ui.home.adapter.HomeAdapter


class EditorShopsViewHolder(
    val binding: CellEditorShopViewPagerItemBinding,
    val liveData: MutableLiveData<HomeAdapter.State>
) : BaseViewHolder<ArrayList<MainResponse>>(binding.root) {

    override fun bind(data: ArrayList<MainResponse>) {

        val snapHelper = LinearSnapHelper()
        // val adapter = EditorShopPagerAdapter(binding.root.context, data[4].shops!!)
        snapHelper.attachToRecyclerView(binding.cellEditorShopsRecyclerView)
        binding.cellEditorShopsRecyclerView.adapter = EditorShopAdapter(data[4].shops!!)
        binding.cellEditorShopsTitleTextView.text = data[4].title

        Glide.with(binding.root.context).load(data[4].shops!![0].cover?.url.toString())
            .into(binding.cellEditorShopsBackgroundImageView)
        binding.cellEditorShopsRecyclerView.addOnScrollListener(object :
            RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                when (newState) {
                    RecyclerView.SCROLL_STATE_IDLE -> {
                        val positionView =
                            (recyclerView.layoutManager as LinearLayoutManager?)!!.findFirstCompletelyVisibleItemPosition()
                        if (positionView > -1)
                            Glide.with(binding.root.context)
                                .load(data[4].shops!![positionView].cover?.url.toString())
                                .into(binding.cellEditorShopsBackgroundImageView)
                    }
                }
            }
        })

        binding.cellEditorShopsAllTextView.setOnClickListener {
            liveData.value = HomeAdapter.State.OnAllClick(data, data[4].type)
        }

    }

}