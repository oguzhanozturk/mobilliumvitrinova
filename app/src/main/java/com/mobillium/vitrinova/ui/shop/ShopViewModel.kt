package com.mobillium.vitrinova.ui.shop

import android.os.Bundle
import com.mobillium.vitrinova.ui.base.BaseViewModel
import com.mobillium.vitrinova.network.model.MainResponse
import com.mobillium.vitrinova.util.ListType
import javax.inject.Inject

class ShopViewModel @Inject constructor() : BaseViewModel() {
    var mainResponse : ArrayList<MainResponse>? = null
    var type: String? = ListType.EDITOR_SHOPS.type
    override fun handleIntent(extras: Bundle) {
        type = extras.getString("type") ?: ListType.EDITOR_SHOPS.type
        mainResponse = extras.getParcelableArrayList(ShopActivity.EXTRA_SERVICE_ITEM) ?: arrayListOf()
    }
}