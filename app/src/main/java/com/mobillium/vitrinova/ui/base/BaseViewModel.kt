package com.mobillium.vitrinova.ui.base

import android.os.Bundle
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BaseViewModel(
    private val compositeDisposable: CompositeDisposable = CompositeDisposable(),
    val baseLiveData: MutableLiveData<State> = MutableLiveData()
) : ViewModel() {
    open fun handleIntent(extras: Bundle) {}
    fun getCompositeDisposable() = compositeDisposable

    fun Disposable.track() {
        compositeDisposable.add(this)
    }

    inline fun <T> Single<T>.sendRequest(
        requestType: RequestType = RequestType.CUSTOM,
        crossinline successHandler: (T) -> Unit
    ) {
        baseLiveData.value =
            State.ShowLoading(
                requestType
            )
        subscribe(
            {
                successHandler(it)
            },
            {
                baseLiveData.value =
                    State.OnError(
                        it,
                        requestType
                    )
            })
            .track()
    }


    sealed class State {
        data class OnError(val throwable: Throwable, val requestType: RequestType) : State()
        data class ShowLoading(val requestType: RequestType? = null) : State()
        object ShowEmpty : State()
    }

    enum class RequestType {
        INIT,
        ACTION,
        CUSTOM
    }

    override fun onCleared() {
        compositeDisposable.clear()
    }

}