package com.mobillium.vitrinova.ui.home.adapter

import com.mobillium.vitrinova.R
import com.mobillium.vitrinova.databinding.CellProductItemBinding
import com.mobillium.vitrinova.ui.base.BaseAdapter
import com.mobillium.vitrinova.network.model.Product

class ProductAdapter (
    products: ArrayList<Product>
): BaseAdapter<CellProductItemBinding, Product>(R.layout.cell_product_item) {

    init {
        items = products
    }

    override fun bindView(binding: CellProductItemBinding, item: Product?, adapterPosition: Int) {
        binding.product = item
        binding.image = item!!.images!![0]

    }

}

