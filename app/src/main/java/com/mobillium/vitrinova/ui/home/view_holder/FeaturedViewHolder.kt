package com.mobillium.vitrinova.ui.home.view_holder

import com.mobillium.vitrinova.ui.base.BaseViewHolder
import com.mobillium.vitrinova.databinding.CellFeaturedItemBinding
import com.mobillium.vitrinova.network.model.MainResponse
import com.mobillium.vitrinova.ui.home.adapter.HomePagerAdapter

class FeaturedViewHolder(
    val binding: CellFeaturedItemBinding
) : BaseViewHolder<ArrayList<MainResponse>>(binding.root) {

    override fun bind(data: ArrayList<MainResponse>) {
        val adapter = HomePagerAdapter(this.context, data)
        binding.cellFeaturedItemViewPager.adapter = adapter
        binding.cellFeaturedItemViewPager.translationX =
            (-1 * binding.cellFeaturedItemViewPager.width * position).toFloat()
        binding.cellFeaturedItemViewPager.setPageTransformer(true) { page, position ->
            val pageWidth = page.width
            when {
                position < -1 -> { // [-Infinity,-1)
                    // This page is way off-screen to the left.
                    page.alpha = 0f
                }
                position <= 0 -> { // [-1,0]
                    // Use the default slide transition when moving to the left page
                    page.alpha = 1f
                    page.translationX = 0f
                    page.scaleX = 1f
                    page.scaleY = 1f
                }
                position <= 1 -> { // (0,1]
                    // Fade the page out.
                    page.alpha = 1 - position

                    // Counteract the default slide transition
                    page.translationX = pageWidth * -position

                    // Scale the page down (between MIN_SCALE and 1)
                    val scaleFactor = (0.75f + (1 - 0.75f) * (1 - kotlin.math.abs(position)))
                    page.scaleX = scaleFactor
                    page.scaleY = scaleFactor
                }
                else -> { // (1,+Infinity]
                    // This page is way off-screen to the right.
                    page.alpha = 0f
                }
            }
        }
    }

}