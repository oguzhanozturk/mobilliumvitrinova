package com.mobillium.vitrinova.ui.splash

import androidx.lifecycle.MutableLiveData
import com.mobillium.vitrinova.ui.base.BaseViewModel
import com.mobillium.vitrinova.network.ApiDataManager
import com.mobillium.vitrinova.network.model.MainResponse
import javax.inject.Inject

class SplashViewModel @Inject constructor(private val apiDataManager: ApiDataManager) : BaseViewModel() {

    var liveData = MutableLiveData<State>()

    fun discover() {
        apiDataManager.discover().sendRequest {
            liveData.value = State.onLoadingState(it)
        }
    }

    sealed class State{
        data class onLoadingState(val mainResponse: ArrayList<MainResponse>): State()
    }
}