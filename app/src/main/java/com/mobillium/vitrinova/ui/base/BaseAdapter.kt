package com.mobillium.vitrinova.ui.base

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.mobillium.vitrinova.R

abstract class BaseAdapter<T : ViewDataBinding, V>(@LayoutRes val layout: Int) :
    RecyclerView.Adapter<BaseAdapter.BaseHolder<T, V>>() {

    var items: ArrayList<V>? = ArrayList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    lateinit var layoutInflater: LayoutInflater
    var binding: T? = null

    override fun onCreateViewHolder(parent: ViewGroup, container: Int): BaseHolder<T, V> {
        layoutInflater = LayoutInflater.from(parent.context)
        binding = DataBindingUtil.inflate(layoutInflater, layout, parent, false)
        return BaseHolder(binding!!)
    }

    private fun setAnimation(view: View) {
        val animation = AnimationUtils.loadAnimation(view.context, R.anim.item_animation)
        view.startAnimation(animation)
    }


    override fun getItemCount() = items?.size ?: 0

    override fun onBindViewHolder(holder: BaseHolder<T, V>, position: Int) {
        setAnimation(holder.itemView)
        holder.bind(items?.get(position), this)
    }

    class BaseHolder<T : ViewDataBinding, V>(val binding: T) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: V?, adapter: BaseAdapter<T, V>) {
            adapter.bindView(binding, item, adapterPosition)
        }
    }

    abstract fun bindView(binding: T, item: V?, adapterPosition: Int)

}