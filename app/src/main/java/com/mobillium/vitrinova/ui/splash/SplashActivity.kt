package com.mobillium.vitrinova.ui.splash

import com.mobillium.vitrinova.R
import com.mobillium.vitrinova.ui.base.BaseActivity
import com.mobillium.vitrinova.databinding.ActivitySplashBinding
import com.mobillium.vitrinova.ui.home.HomeActivity
import com.mobillium.vitrinova.util.extension.observe

class SplashActivity(
    override val layoutResourceId: Int = R.layout.activity_splash,
    override val classTypeOfViewModel: Class<SplashViewModel> = SplashViewModel::class.java
) : BaseActivity<ActivitySplashBinding, SplashViewModel>() {

    override fun initialize() {
        observe(viewModel.liveData, ::onStateChanged)
    }

    override fun initStartRequest() {
        viewModel.discover()
    }

    private fun onStateChanged(state: SplashViewModel.State) {
        when (state) {
            is SplashViewModel.State.onLoadingState -> {
                HomeActivity.start(this, state.mainResponse)
            }
        }
    }
}
