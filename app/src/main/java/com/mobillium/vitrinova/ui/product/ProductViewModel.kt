package com.mobillium.vitrinova.ui.product

import android.os.Bundle
import com.mobillium.vitrinova.ui.base.BaseViewModel
import com.mobillium.vitrinova.network.model.MainResponse
import javax.inject.Inject

class ProductViewModel @Inject constructor() : BaseViewModel() {

    var mainResponse : ArrayList<MainResponse>? = null
    override fun handleIntent(extras: Bundle) {
        mainResponse = extras.getParcelableArrayList(ProductActivity.EXTRA_SERVICE_ITEM) ?: arrayListOf()
    }
}