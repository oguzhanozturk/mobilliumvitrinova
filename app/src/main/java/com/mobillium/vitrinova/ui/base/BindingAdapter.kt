package com.mobillium.vitrinova.ui.base

import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.mobillium.vitrinova.R
import com.mobillium.vitrinova.util.ImageLoaderUtiliy

@BindingAdapter("url")
fun loadImage(
    view: ImageView,
    imageUrl: String
) {
    ImageLoaderUtiliy.imageLoaderWithCacheAndPlaceHolder(
        view.context,
        imageUrl,
        view,
        R.mipmap.ic_launcher_round
    )
}

@BindingAdapter("layoutMarginEnd")
fun setLayoutMarginBottom(view: View, dimen: Float) {
    val layoutParams = view.layoutParams as ViewGroup.MarginLayoutParams
    layoutParams.rightMargin = dimen.toInt()
    view.layoutParams = layoutParams
}
