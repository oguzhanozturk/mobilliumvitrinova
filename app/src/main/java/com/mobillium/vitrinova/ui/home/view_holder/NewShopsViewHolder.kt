package com.mobillium.vitrinova.ui.home.view_holder

import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.LinearSnapHelper
import com.mobillium.vitrinova.ui.base.BaseViewHolder
import com.mobillium.vitrinova.databinding.CellHorizantalViewBinding
import com.mobillium.vitrinova.network.model.MainResponse
import com.mobillium.vitrinova.ui.home.adapter.HomeAdapter
import com.mobillium.vitrinova.ui.home.adapter.ShopAdapter

class NewShopsViewHolder(
    val binding: CellHorizantalViewBinding,
    val liveData: MutableLiveData<HomeAdapter.State>
) : BaseViewHolder<ArrayList<MainResponse>>(binding.root) {

    override fun bind(data: ArrayList<MainResponse>) {
        binding.cellHorizantalViewTitleTextView.text = data[5].title

        val snapHelper = LinearSnapHelper()
        snapHelper.attachToRecyclerView(binding.cellHorizantalViewRecyclerView)
        binding.cellHorizantalViewRecyclerView.adapter = ShopAdapter(data[5].shops!!)

        binding.cellHorizantalViewAllTextView.setOnClickListener {
            liveData.value = HomeAdapter.State.OnAllClick(data, data[5].type)
        }
    }

}