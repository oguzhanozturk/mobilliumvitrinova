package com.mobillium.vitrinova.ui.base

import com.mobillium.vitrinova.network.service.ApiServices

interface BaseServices {
    fun getApiServices(): ApiServices

}