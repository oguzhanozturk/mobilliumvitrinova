package com.mobillium.vitrinova.ui.home.adapter

import com.mobillium.vitrinova.R
import com.mobillium.vitrinova.databinding.CellCategoryItemBinding
import com.mobillium.vitrinova.ui.base.BaseAdapter
import com.mobillium.vitrinova.network.model.Category

class CategoryAdapter(
    products: ArrayList<Category>
) : BaseAdapter<CellCategoryItemBinding, Category>(R.layout.cell_category_item) {

    init {
        items = products
    }

    override fun bindView(
        binding: CellCategoryItemBinding,
        item: Category?,
        adapterPosition: Int
    ) {
        binding.category = item
        binding.image = item!!.logo!!
    }

}

