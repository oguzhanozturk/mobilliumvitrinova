package com.mobillium.vitrinova.ui.home.view_holder

import androidx.lifecycle.MutableLiveData
import com.mobillium.vitrinova.ui.base.BaseViewHolder
import com.mobillium.vitrinova.databinding.CellHorizantalViewBinding
import com.mobillium.vitrinova.network.model.MainResponse
import com.mobillium.vitrinova.ui.home.adapter.CollectionAdapter
import com.mobillium.vitrinova.ui.home.adapter.HomeAdapter


class CollectionsViewHolder(
    val binding: CellHorizantalViewBinding,
    val liveData: MutableLiveData<HomeAdapter.State>
): BaseViewHolder<ArrayList<MainResponse>>(binding.root){
    override fun bind(data: ArrayList<MainResponse>) {
        binding.cellHorizantalViewRecyclerView.adapter = CollectionAdapter(data[3].collections!!)
        binding.cellHorizantalViewTitleTextView.text = data[3].title

        binding.cellHorizantalViewAllTextView.setOnClickListener {
            liveData.value = HomeAdapter.State.OnAllClick(data, data[3].type)
        }
    }

}