package com.mobillium.vitrinova.ui.home.adapter

import com.mobillium.vitrinova.R
import com.mobillium.vitrinova.ui.base.BaseAdapter
import com.mobillium.vitrinova.databinding.CellCollectionsItemBinding
import com.mobillium.vitrinova.network.model.Collection


class CollectionAdapter (
    collections: ArrayList<Collection>
): BaseAdapter<CellCollectionsItemBinding, Collection>(R.layout.cell_collections_item) {

    init {
        items = collections
    }

    override fun bindView(binding: CellCollectionsItemBinding, item: Collection?, adapterPosition: Int) {
        binding.collections = item
        binding.image = item!!.cover
    }

}