package com.mobillium.vitrinova.ui.shop

import android.app.Activity
import android.content.Intent
import com.mobillium.vitrinova.R
import com.mobillium.vitrinova.databinding.ActivityShopBinding
import com.mobillium.vitrinova.ui.base.BaseActivity
import com.mobillium.vitrinova.network.model.MainResponse
import com.mobillium.vitrinova.ui.shop.adapter.ShopAdapter

class ShopActivity (
    override val layoutResourceId: Int = R.layout.activity_shop,
    override val classTypeOfViewModel: Class<ShopViewModel> = ShopViewModel::class.java
) : BaseActivity<ActivityShopBinding, ShopViewModel>() {

    lateinit var adapter: ShopAdapter

    override fun initialize() {

        if (viewModel.type.equals("editor_shops")){
            binding.title = viewModel.mainResponse!![4].title
            adapter = ShopAdapter(
                viewModel.mainResponse!![4].shops!!
            )
        }else {
            binding.title = viewModel.mainResponse!![5].title
            adapter = ShopAdapter(
                viewModel.mainResponse!![5].shops!!
            )
        }

        binding.activityShopRecyclerView.adapter = adapter

        binding.shopDetailToolbarBackButton.setOnClickListener {
            onBackPressed()
        }
    }

    companion object{
        const val EXTRA_SERVICE_ITEM = "com.mobillium.interview.ui.shop_detail.extra_service_item"
        fun start(activity: Activity, mainResponse: ArrayList<MainResponse>, type: String) {
            activity.startActivity(Intent(activity, ShopActivity::class.java).apply {
                putParcelableArrayListExtra(EXTRA_SERVICE_ITEM, mainResponse)
                putExtra("type", type)
            })
        }
    }

}
