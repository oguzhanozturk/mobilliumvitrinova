package com.mobillium.vitrinova.network.model


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MainResponse(
    @SerializedName("type")
    var type: String? = null,
    @SerializedName("title")
    var title: String? = null,
    @SerializedName("featured")
    var featured: ArrayList<Featured>? = null,
    @SerializedName("products")
    var products: ArrayList<Product>? = null,
    @SerializedName("categories")
    var categories: ArrayList<Category>? = null,
    @SerializedName("collections")
    var collections: ArrayList<Collection>? = null,
    @SerializedName("shops")
    var shops: ArrayList<Shop>? = null
):Parcelable