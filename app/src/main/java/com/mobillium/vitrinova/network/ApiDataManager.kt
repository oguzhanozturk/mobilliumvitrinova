package com.mobillium.vitrinova.network

import com.mobillium.vitrinova.network.model.MainResponse
import com.mobillium.vitrinova.network.service.ApiServices
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ApiDataManager @Inject constructor(private val apiServices: ApiServices){

    fun discover(): Single<ArrayList<MainResponse>> {
        return apiServices.discover().applySchedulers()
    }

    fun <T> Single<T>.applySchedulers(): Single<T> {
        return subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
    }
}