package com.mobillium.vitrinova.network.model


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Cover(
    @SerializedName("width")
    var width: Int? = null,
    @SerializedName("height")
    var height: Int? = null,
    @SerializedName("url")
    var url: String? = null,
    @SerializedName("medium")
    var medium: Medium? = null,
    @SerializedName("thumbnail")
    var thumbnail: Thumbnail? = null
):Parcelable