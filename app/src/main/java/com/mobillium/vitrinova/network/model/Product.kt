package com.mobillium.vitrinova.network.model


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Product(
    @SerializedName("id")
    var id: Int? = null,
    @SerializedName("code")
    var code: String? = null,
    @SerializedName("title")
    var title: String? = null,
    @SerializedName("slug")
    var slug: String? = null,
    @SerializedName("definition")
    var definition: String? = null,
    @SerializedName("old_price")
    var oldPrice: Int? = null,
    @SerializedName("price")
    var price: Int? = null,
    @SerializedName("stock")
    var stock: Int? = null,
    @SerializedName("max_installment")
    var maxInstallment: String? = null,
    @SerializedName("commission_rate")
    var commissionRate: Int? = null,
    @SerializedName("cargo_time")
    var cargoTime: Int? = null,
    @SerializedName("is_cargo_free")
    var isCargoFree: Boolean? = null,
    @SerializedName("is_new")
    var isNew: Boolean? = null,
    @SerializedName("reject_reason")
    var rejectReason: String? = null,
    @SerializedName("category_id")
    var categoryId: Int? = null,
    @SerializedName("difference")
    var difference: String? = null,
    @SerializedName("is_editor_choice")
    var isEditorChoice: Boolean? = null,
    @SerializedName("comment_count")
    var commentCount: Int? = null,
    @SerializedName("is_owner")
    var isOwner: Boolean? = null,
    @SerializedName("is_approved")
    var isApproved: Boolean? = null,
    @SerializedName("is_active")
    var isActive: Boolean? = null,
    @SerializedName("share_url")
    var shareUrl: String? = null,
    @SerializedName("is_liked")
    var isLiked: Boolean? = null,
    @SerializedName("like_count")
    var likeCount: Int? = null,
    @SerializedName("shop")
    var shop: Shop? = null,
    @SerializedName("category")
    var category: Category? = null,
    @SerializedName("images")
    var images: ArrayList<Image>? = null
):Parcelable