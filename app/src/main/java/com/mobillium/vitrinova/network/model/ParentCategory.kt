package com.mobillium.vitrinova.network.model


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ParentCategory(
    @SerializedName("id")
    var id: Int? = null,
    @SerializedName("name")
    var name: String? = null,
    @SerializedName("parent_id")
    var parentId: String? = null,
    @SerializedName("order")
    var order: Int? = null,
    @SerializedName("parent_category")
    var parentCategory: String? = null
):Parcelable