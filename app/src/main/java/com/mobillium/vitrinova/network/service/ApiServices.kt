package com.mobillium.vitrinova.network.service

import com.mobillium.vitrinova.network.model.MainResponse
import io.reactivex.Single
import retrofit2.http.GET

interface ApiServices {

    @GET("api/v2/discover")
    fun discover(): Single<ArrayList<MainResponse>>
}