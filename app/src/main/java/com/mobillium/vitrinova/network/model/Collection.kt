package com.mobillium.vitrinova.network.model


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Collection(
    @SerializedName("id")
    var id: Int? = null,
    @SerializedName("title")
    var title: String? = null,
    @SerializedName("definition")
    var definition: String? = null,
    @SerializedName("start")
    var start: String? = null,
    @SerializedName("end")
    var end: String? = null,
    @SerializedName("share_url")
    var shareUrl: String? = null,
    @SerializedName("cover")
    var cover: Cover? = null,
    @SerializedName("logo")
    var logo: Logo? = null
):Parcelable