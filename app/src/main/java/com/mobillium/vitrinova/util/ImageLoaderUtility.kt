package com.mobillium.vitrinova.util

import android.content.Context
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy

class ImageLoaderUtiliy {

    companion object {
        fun imageLoaderWithCacheAndPlaceHolder(
            context: Context,
            imageUrl: String? = "",
            imageView: ImageView,
            placeholder: Int
        ) {
            if (!imageUrl.isNullOrEmpty()) {
                Glide.with(context)
                    .load(imageUrl)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(placeholder)
                    .into(imageView)
            } else {
                Glide.with(context)
                    .load(placeholder)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imageView)
            }
        }
    }
}