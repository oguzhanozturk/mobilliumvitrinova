package com.mobillium.vitrinova.util.extension

import android.app.Activity
import android.util.Patterns
import android.view.Menu
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import androidx.annotation.IdRes
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.material.bottomnavigation.BottomNavigationView
import java.text.DecimalFormat



fun View.setPushDown(onclick: () -> Unit) =  run {
    /*PushDownAnim.setPushDownAnimTo(this).setScale(PushDownAnim.MODE_STATIC_DP,5F).setOnClickListener {
        onclick.invoke()
    }*/
}

fun String.isValidEmail():Boolean{
    return Patterns.EMAIL_ADDRESS.matcher(this).matches()
}


fun Double.toPriceTLSembol(): String {
    val price = this
    val decimalFormat = DecimalFormat()

    var priceTL = decimalFormat.format(price).toString()

    priceTL += " ₺"
    return priceTL
}

fun Double.toPercentage(): String {
    return "% $this"
}

fun View.show() {
    this.visibility = View.VISIBLE
}

fun View.gone() {
    this.visibility = View.GONE
}

fun View.invisible() {
    this.visibility = View.INVISIBLE
}

fun EditText.onSubmit(func: () -> Unit) {
    setOnEditorActionListener { _, actionId, _ ->

        if (actionId == EditorInfo.IME_ACTION_DONE) {
            func()
        }

        true

    }
}


