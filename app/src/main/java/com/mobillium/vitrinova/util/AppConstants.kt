package com.mobillium.vitrinova.util

/**
 ** Created by oguzhanozturk on 10.09.2020.
 */
class AppConstants {
    companion object {
        // Network Configuration
        const val READ_TIME_OUT: Long = 60
        const val CONNECT_TIME_OUT: Long = 60

        const val  BASE_URL=""

        const val REGISTER_ERROR_CODE: Int = 1001

    }
}