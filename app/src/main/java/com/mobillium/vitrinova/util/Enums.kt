package com.mobillium.vitrinova.util

enum class ListType(val type: String) {
    FEATURED("featured"),
    NEW_PRODUCTS("new_products"),
    CATEGORIES("categories"),
    COLLECTIONS("collections"),
    EDITOR_SHOPS("editor_shops"),
    NEW_SHOPS("new_shops")
}